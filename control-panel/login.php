<?php
require_once("../includes/config.inc.php");

$pageTitle = "Login";
$pageDescription = "";

if($_SERVER['REQUEST_METHOD'] == "POST") {
    $userNameEntered = $_POST['txtUserName'] ?? NULL;
    $passwordEntered = $_POST['txtPassword'] ?? NULL;

    $userName = "test";
    $password = "thisisfortesting";

    if($userNameEntered == $userName && $passwordEntered == $password) {
        session_regenerate_id(true);
        $_SESSION['authenticated'] = "yes";

        header("Location: index.php");
        exit();
    }
} elseif ($_SERVER['REQUEST_METHOD'] =="GET") {
    // destroy the session when user visits login page
    //the session will get created again when they log in

    if(isset($_COOKIE[session_name()])) {
        setcookie( session_name(), "", time()-3600, "/");
    }

    $_SESSION = array();
    session_destroy();
}

require("../includes/header.inc.php");
?>
<main>
	<div class="content-frame">
		<h3>Login</h3>
		<form method="POST" action="">
			<label for="txtUserName">User Name</label>
			<br>
			<input type="text" name="txtUserName" id="txtUserName" />
			<br>
			<label for="txtPassword">Password</label>
			<br>
			<input type="password" name="txtPassword" id="txtPassword" />
			<br>
			<input type="submit" value="Log In">
		</form>
	</div>
</main>
		
<?php
if(!empty($sideBar)){
	require("../includes/" . $sideBar);
}

require("../includes/footer.inc.php");
?>