<?php
require_once("includes/config.inc.php");
$pageTitle = "Contact";
$pageDescription = "This page will allow you to contact me!";
$sideBar = "hobbies-sidebar.inc.php";

if($_SERVER['REQUEST_METHOD'] == "POST") {
	// Get the data entered by the user
	$firstName = $_POST['txtFirstName'] ?? "";
	$lastName = $_POST['txtLastName'] ?? "";
	$email = $_POST['txtEmail'] ?? "";
	$comments = $_POST['txtComments'] ?? "";

	if(validateContactData($firstName, $lastName, $email, $comments)) {
		//If the data is valid, then put it into a single string to send as an email
		$msg = "Name: $firstName $lastName <br>";
		$msg .= "Email: $email <br>";
		$msg .= "Comments: $comment";

		sendEmail(ADMIN_EMAIL, "Contact Form", $msg);
		header("Location: " . PROJECT_DIR . "contact-confirmation.php");
		exit();
	} else {
		//Foul play suspected (client-side validation has been bypassed)!
		$msg = getAllSuperGlobals();
		sendEmail(ADMIN_EMAIL, "Security Warning!", $msg);
		header("Location: " . PROJECT_DIR . "error.php");
		exit();
	}
}


require("includes/header.inc.php");

?>
<script type="text/javascript" src="<?php echo(PROJECT_DIR); ?>js/contact.js"></script>
<main>

	<div class="content-frame">
		
		<h1>Contact Me</h1>
		
		<form id="contactForm" method="POST" action="">
			<table border="1">
				<tr>
					<td align="right" valign="bottom">First Name:</td>
					<td>
						<div class="validation-message" id="vFirstName"></div>
						<input type="text" id="txtFirstName" name="txtFirstName">
					</td>
				</tr>
				<tr>
					<td align="right" valign="bottom">Last Name:</td>
					<td>
						<div class="validation-message" id="vLastName"></div>
						<input type="text" id="txtLastName" name="txtLastName">
					</td>
				</tr>
				<tr>
					<td align="right" valign="bottom">Email:</td>
					<td>
						<div class="validation-message" id="vEmail"></div>
						<input type="text" id="txtEmail" name="txtEmail">
					</td>
				</tr>
				<tr>
					<td align="right" valign="top">Comments:</td>
					<td>
						<div class="validation-message" id="vComments"></div>
						<textarea id="txtComments" name="txtComments"></textarea>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;</td>
					<td>
						<input type="submit" value="SUBMIT">
					</td>
				</tr>
			</table>
		</form>

	</div>
	
</main>
<?php
if(!empty($sideBar)) {
	require("includes/" . $sideBar);
}

require("includes/footer.inc.php");

function validateContactData($firstName, $lastName, $email, $comments) {
	// Make sure that none to the inputs are empty
	if(empty($firstName) || empty($lastName) || empty($comments) || empty($email)) {
		return false;
	}

	// Make sure the email entered is a vaild one (you may wnat to look up the
	// filter_var() function in PHP)
	if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		return false;
	}

	return true;
}
?>

